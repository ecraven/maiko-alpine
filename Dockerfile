FROM alpine:latest
RUN apk update && apk add git make clang musl-dev libx11-dev binutils gcc && git clone https://github.com/Interlisp/maiko.git && cd maiko/bin && sed '24i #include <time.h>' -i ../src/initsout.c && ./makeright x && mv ../linux.x86_64/* /usr/local/bin && cd && rm -Rf maiko && apk del git make clang musl-dev libx11-dev binutils gcc && apk add libx11
COPY start-medley.sh /root/start-medley.sh
