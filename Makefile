.PHONY: run build
run:
	podman --cgroup-manager cgroupfs run -ti --rm -p 5900:5900/tcp maiko sh
build:
	podman --cgroup-manager cgroupfs build . -t maiko
