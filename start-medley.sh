#!/bin/sh
apk add git xvfb x11vnc
cd /root
git clone --depth 1 https://github.com/Interlisp/medley.git
Xvfb &
export DISPLAY=:0
sleep 1
x11vnc &
sleep 1
echo Starting Medley...
ldex -g 1440x900 -sc 1440x900 -m 256  /root/medley/loadups/full.sysout
